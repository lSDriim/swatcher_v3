const log4js = require('log4js')

log4js.configure({
  appenders: { ClearSubscriptions: { type: 'stdout' } },
  categories: { default: { appenders: ['ClearSubscriptions'], level: 'debug' } }
})

const logger = log4js.getLogger('ClearSubscriptions')

module.exports = function (user, subs) {
  return new Promise(function (resolve, reject) {
    subs.updateMany({ 'fans.user': user._id },
      { $pull: { fans: { user: user._id } } })
      .then((result) => {
        logger.info(`Removed ${result.nModified} subscriptions of user ${user.id}`)

        resolve()
      })
      .catch((err) => {
        reject(err)
      })
  })
}