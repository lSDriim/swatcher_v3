const log4js = require('log4js')
const Fusejs = require('fuse.js')
const escapeRegexp = require('escape-string-regexp')

const Serial = require('./models/serial')

const HelpString = require('./text').help
const FreeSubscribe = require('./text').subs
const UserNotExist = require('./text').user

/*
 * Здесь будет весь UI бота, причем в отличие от 2 версии,
 * этот класс так же будет включать функционал TelegramBotWrapper.
 *
 * Из нового здесь:
 *    - выбор озвучек и сессии
 */

const MAX_SEARCH_COUNT = 4
const MAX_FREE_SERIALS = 2

class UserNotFoundError extends Error {
  constructor () {
    super()
    this.name = 'UserNotFoundError'
    this.message = 'Unable to found user'
  }
}

class SerialNotFoundError extends Error {
  constructor () {
    super()
    this.name = 'SerialNotFoundError'
    this.message = 'Unable to found serial'
  }
}

class LimitExceedError extends Error {
  constructor () {
    super()
    this.name = 'LimitExceedError'
    this.message = 'Limit of free subscribes exceed'
  }
}

class NotSubscribedError extends Error {
  constructor () {
    super()
    this.name = 'NotSubscribedError'
    this.message = 'User not subscribed to serial'
  }
}

log4js.configure({
  appenders: { SwatcherUI: { type: 'stdout' } },
  categories: { default: { appenders: ['SwatcherUI'], level: 'debug' } }
})

class BotUI {
  /*
   * @class TelegramBotWrapper
   * @constructor
   * @param {Object} [bot] Bot interface.
   * @param {Object} [errorBot] interface to send error messages,
   *                            it must have one feature - sendMessage.
   * @param {Object} [model] interface to user db.
   * @param {Object} [subscription] interface to subscriptions db.
   */
  constructor (bot, errorBot, model, subscription) {
    if (!bot || !errorBot || !model || !subscription) {
      throw new Error('all params must be set')
    }

    if (!('sendMessage' in bot || 'sendMessage' in errorBot)) {
      throw new Error('wrong bot or error_bot')
    }

    this.bot = bot
    this.errorBot = errorBot
    this.logger = log4js.getLogger('SwatcherUI')
    this.model = model
    this.subscription = subscription
  }

  setFuseList (list) {
    if (!list) {
      this.logger.error('Установка пустого списка для неточного поиска')
      return
    }

    const opts = {
      shouldSort: true,
      threshold: 0.4,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 2,
      keys: [
        'name',
        'alias'
      ]
    }

    this.fuse = new Fusejs(list, opts)
  }

  setDashbot (dashbot) {
    if (!dashbot) {
      return
    }

    this.dashbot = dashbot
  }

  trackIncoming (message) {
    if (!message || !this.dashbot) {
      this.logger.error('Не удалось сохранить трэк входящего сообщения')
      return
    }

    this.dashbot.logIncoming(message)
  }

  trackOutgoing (message) {
    if (!message || !this.dashbot) {
      this.logger.error('Не удалось сохранить трэк исходящего сообщения')
      return
    }

    this.dashbot.logOutgoing(message)
  }

  handleBlock (fan, error, resolve) {
    const self = this
    const res = error.toString().match(/Error: ETELEGRAM:\s(\d+)/)

    self.logger.error(`Ошибка посылки сообщения пользователю: ${error}`)
    if (res !== null && res[1] === '403') {
      self.logger.warn(`Бот был заблокирован пользователем: ${fan.id}`)

      fan.active = 0
      return fan.save()
        .then(() => {
          if (fan.clearSubscriptions !== undefined) {
            return fan.clearSubscriptions()
          }
        })
        .then(() => resolve())
        .catch((error) => {
          self.logger.error(`Бот был заблокирован пользователем и мы не смогли его сохранить: ${fan.id}: ${error}`)
          resolve()
        })
    } else {
      resolve()
    }
  }

  sendMessage (fan, message, opts) {
    const self = this
    const info = {
      text: message,
      userId: fan.id,
      platformJson: opts
    }

    this.trackOutgoing(info)

    return new Promise((resolve, reject) => {
      self.bot.sendMessage(fan.id, message, opts)
        .then(() => {
          resolve()
        })
        .catch((error) => {
          self.handleBlock(fan, error, resolve)
        })
    })
  }

  sendSimpleAnswer (msg, answer, opts) {
    const id = msg.chat.id
    const self = this

    if (!id) {
      self.logger.error('SimpleAnswer: Не найден chat ID в сообщение')
      return
    }

    self.model.findOne({ id })
      .then((user) => {
        if (user === null) {
          self.sendMessage({ id }, UserNotExist)
        } else {
          self.sendMessage(user, answer, opts)
        }
      })
      .catch((error) => {
        self.logger.error(`SimpleAnswer: не удалось послать собщение пользователю ${id}: ${error}`)
      })
  }

  sendHelpString (msg, match) {
    this.sendSimpleAnswer(msg, HelpString)
  }

  sendUserNoThanks (msg, match) {
    /* clear custom keyboard */
    const opts = {
      reply_markup: JSON.stringify({ remove_keyboard: true })
    }

    this.sendSimpleAnswer(msg, 'Ok!', opts)
  }

  sendId (msg, match) {
    this.sendSimpleAnswer(msg, `${msg.chat.id}`)
  }

  startUser (msg, match) { /* Rewrite using custom Errors */
    const self = this
    const Model = this.model
    let returned = false
    let oldOne = false

    const fan = new Model({
      id: Number(msg.chat.id),
      username: msg.chat.username,
      active: 1,
      subscriptions: 0
    })

    self.trackIncoming({ text: match, userId: msg.chat.id })

    self.model.findOne({ id: msg.chat.id })
      .then((result) => {
        if (result === null) {
          /* new user */
          return fan.save()
        } else if (result.active !== 1) {
          /* returning user */
          returned = true
          result.active = 1
          return result.save()
        } else {
          /* already exist */
          oldOne = true
          return result
        }
      })
      .then((user) => {
        if (oldOne) {
          self.logger.info('Пользователь уже существует')
          self.sendMessage(user, `${user.username} вы уже зарегистрированны в системе.`)
        } else if (returned) {
          self.logger.info('Пользователь вернулся')
          self.sendMessage(user, `${user.username} я рад что вы снова с нами!`)
        } else {
          self.logger.info('Регистрация нового пользователя')
          self.sendMessage(user, `${user.username} вы были зарегистрированны в системе, теперь вам нужно добавить отслеживаемые вами сериалы. Просто введите название интересующего вас сериала. Все вопросы вы можете задать здесь: https://t.me/swatcher_group_origin`)
        }
      })
      .catch((error) => {
        self.logger.error(`ошибка ${error}`)
        self.sendMessage({ id: msg.chat.id }, `${msg.chat.username} произошла ошибка, попробуйте снова или обратитесь к администратору группы.`)
        self.errorBot.sendMessage(`Для ${msg.chat.id} вернуло ошибку ${error}`)
      })
  }

  stopUser (msg, match) { /* Rewrite using custom Errors */
    const self = this

    self.trackIncoming({ text: match, userId: msg.chat.id })

    self.model.findOne({ id: msg.chat.id })
      .then((user) => {
        if (user === null || user.active === 0) {
          return null
        }

        user.active = 0
        return user.save()
      })
      .then((user) => {
        if (user === null) {
          self.logger.info(`Попытка удаления не существующего пользователя: ${msg.chat.id}`)
          self.sendMessage({ id: msg.chat.id }, 'Вы не были зарегистрированы!')
        } else {
          user.clearSubscriptions()
          self.sendMessage(user, 'Что ж, пока!')
        }
      })
      .catch((error) => {
        self.logger.error(`Ошибка при удалении пользователя ${error}`)
        self.errorBot.sendMessage(`END: для ${msg.chat.id} вернуло ошибку ${error}`)
      })
  }

  getLastSeasonPoster (serial) {
    return serial.season.reduce((a, b) => parseInt(a.name) > parseInt(b.name) ? a : b).img
  }

  sendSerialMessage (fan, serial) {
    const self = this
    const info = {
      text: serial.name,
      userId: fan.id
    }

    self.trackOutgoing(info)
    return new Promise((resolve, reject) => {
      let message = ''

      message += `${serial.name} \n`
      message += `Другие названия: ${serial.alias.join(', ')} \n`
      message += `Страна: ${serial.country.join(', ')} \n`
      message += `Жанр: ${serial.genre.join(', ')} \n`
      message += `Количество сезонов: ${serial.season.length}`

      const img = self.getLastSeasonPoster(serial)
      const opts = {
        caption: message
      }

      self.bot.sendPhoto(fan.id, img, opts)
        .then(() => resolve())
        .catch((error) => {
          self.handleBlock(fan, error, resolve)
        })
    })
  }

  _getMostPopular (serials, max) {
    const self = this

    return new Promise((resolve, reject) => {
      if (serials.length <= max) {
        resolve(serials)
      }

      const ids = serials.map((serial) => serial._id)

      self.subscription.find({
        serial: { $in: ids }
      }).populate('serial')
        .exec()
        .then(subscriptions => {
          if (subscriptions === null || subscriptions.length === 0) {
            /* no one subscribed */
            resolve(serials.slice(0, max))
          } else {
            resolve(subscriptions.sort((a, b) => {
              return b.fans.length - a.fans.length
            }).slice(0, max))
          }
        })
    })
  }

  async _prepareSearchResults (user, serials) {
    const self = this
    const keyboard = [[]]
    let opts

    self._getMostPopular(serials, MAX_SEARCH_COUNT)
      .then(async (popularSerials) => {
        for (let i = 0; i < popularSerials.length; i++) {
          await self.sendSerialMessage(user, popularSerials[i])
          keyboard.push(['Добавить ' + popularSerials[i].name])
        }

        keyboard.push(['Нет, не надо'])
        opts = {
          reply_markup: JSON.stringify({
            keyboard: keyboard,
            one_time_keyboard: true,
            resize_keyboard: true
          })
        }

        if (popularSerials.length !== serials.length) {
          self.sendMessage(user, `Я много нашел(${serials.length} сериалов)! Выдаю вам самые популярные из них. Если тут нет искомого сериала, то уточните запрос.`, opts)
        } else {
          self.sendMessage(user, 'Какой добавить?', opts)
        }
      })
      .catch((error) => {
        self.logger.error(`Ошибка при поиске популярных сериалов для пользователя ${error}`)
        self.errorBot.sendMessage(`PrepareSearch: для ${user.id} вернуло ошибку ${error}`)
      })

    /* always return true, because _fuzzySearch return false if can't find any */
    return true
  }

  async _fuzzySearch (fan, name) {
    if (this.fuse == null) {
      return false
    }

    const result = this.fuse.search(name)

    if (result === null || result.length === 0) {
      return false
    }

    /* Rewrite */
    const ids = result.map((serial) => serial._id)
    const serials = await Serial.find({ _id: { $in: ids } }).exec()

    this._prepareSearchResults(fan, serials)
    return true
  }

  searchUserSerial (msg, match) {
    const self = this
    const serialName = escapeRegexp(match)
    let user

    self.trackIncoming({ text: match, userId: msg.chat.id })

    self.model.findOne({ id: msg.chat.id })
      .then(fan => {
        if (fan === null) {
          throw new UserNotFoundError()
        }

        user = fan
        return Serial
          .find()
          .or([{ name: new RegExp(serialName, 'i') }, { alias: new RegExp(serialName, 'i') }])
      })
      .then(serials => {
        if (serials === null || serials.length === 0) {
          return self._fuzzySearch(user, serialName)
        } else {
          return self._prepareSearchResults(user, serials)
        }
      })
      .then(result => {
        if (!result) {
          self.logger.info(`Не удалось найти сериал ${serialName} для пользователя ${msg.chat.id}`)
          return self.sendMessage(user, 'К сожалению ничего не было найдено!')
        }
      })
      .catch((error) => {
        if (error instanceof UserNotFoundError) {
          self.sendMessage({ id: msg.chat.id }, UserNotExist)
        } else {
          self.logger.error(`Search: Неизвестная ошибка при поиске ${serialName} для пользователя ${msg.chat.id}`)
          self.errorBot.sendMessage(`Search: Для ${msg.chat.id} ошибка ${error}`)
        }
      })
  }

  _getSubscription (id, serialName) {
    let user
    let serial
    const self = this
    const Subscription = this.subscription

    return new Promise((resolve, reject) => {
      self.model.findOne({ id: id })
        .then((fan) => {
          if (fan === null) {
            throw new UserNotFoundError()
          }

          user = fan

          if (user.subscriptions > MAX_FREE_SERIALS && (user.payed === 0 || user.payed === undefined)) {
            self.logger.info(`getSubscription: пользователь: ${user.id} израсходовал лимит подписок`)
            throw new LimitExceedError()
          }

          /* find serial */
          return Serial.findOne({ name: serialName })
        })
        .then((result) => {
          if (result == null) {
            throw new SerialNotFoundError()
          }

          serial = result

          /* find serial subscriptions */
          return self.subscription.findOne({ serial: serial._id })
        })
        .then((subs) => {
          if (subs == null) {
            subs = new Subscription({
              serial: serial._id,
              fans: []
            })
          }

          resolve([subs, serial, user])
        })
        .catch((error) => {
          self.logger.warn(`getSubscription: reject: ${error}`)
          reject(error)
        })
    })
  }

  listUserSerials (msg, match) {
    const self = this
    let user

    self.model.findOne({ id: msg.chat.id })
      .then((fan) => {
        if (fan === null) {
          throw new UserNotFoundError()
        }

        user = fan

        return self.subscription
          .find({ 'fans.user': user._id })
          .populate('serial')
      })
      .then(async (subs) => {
        const keyboard = [[]]

        if (subs.length === 0) {
          throw new NotSubscribedError()
        }

        for (const subscription of subs) {
          await self.sendSerialMessage(user, subscription.serial)
          keyboard.push([`Удалить ${subscription.serial.name}`])
        }

        keyboard.push(['Нет, не надо'])

        const opts = {
          reply_markup: JSON.stringify({
            keyboard: keyboard,
            one_time_keyboard: true,
            resize_keyboard: true
          })
        }

        self.sendMessage(user, 'Вот список сериалов на которые вы подписаны!', opts)
      })
      .catch((error) => {
        if (error instanceof UserNotFoundError) {
          self.sendMessage({ id: msg.chat.id }, UserNotExist)
        } else if (error instanceof NotSubscribedError) {
          self.sendMessage(user, 'Вы не подписаны не на один сериал')
        }
      })
  }

  addUserSerial (msg, match) {
    const self = this
    const serialName = escapeRegexp(match[1])
    let user
    let serial
    let subscription
    let alreadySubscribed

    self.trackIncoming({ text: match, userId: msg.chat.id })

    self._getSubscription(msg.chat.id, serialName)
      .then(([subs, ser, us]) => {
        user = us
        serial = ser
        subscription = subs

        const fan = {
          user: user._id,
          voiceover: []
        }

        /* check if already subscribed, then do nothing because user maybe want to add voiceover */
        alreadySubscribed = subscription.fans.find((element) => { return element.user.equals(user._id) })
        if (alreadySubscribed === undefined) {
          self.logger.info(`ADD: пользователь: ${user.id} подписывается на ${serial.name}`)
          subscription.fans.push(fan)
        }

        return subscription.save()
      })
      .then(() => {
        let opts

        user.subscriptions += 1
        user.save()

        if (user.payed === 1) {
          const keyboard = [[]]

          self.logger.info(`ADD: Подписали платного пользователя: ${user.id} на сериал: ${serial.name}`)

          if (serial.voice_over !== undefined && serial.voice_over.length > 0) {
            for (const voice of serial.voice_over) {
              self.logger.info(`ADD: Добавляем в клавиатуру озвучку от ${voice}`)
              keyboard.push(['В озвучке ' + voice])
            }

            if (alreadySubscribed === undefined) {
              keyboard.push(['В любой озвучке'])
            } else {
              keyboard.push(['Достаточно озвучек'])
            }

            opts = {
              reply_markup: JSON.stringify({
                keyboard: keyboard,
                one_time_keyboard: true,
                resize_keyboard: true
              })
            }

            /* creating context for voiceover subscription commands */
            self._createContext(subscription)

            self.sendMessage(user, `Выберете интересующие вас варианты озвучки сериала ${serial.name}`, opts)
          } else {
            opts = {
              reply_markup: JSON.stringify({ remove_keyboard: true })
            }

            self.sendMessage(user, `Теперь вы подписаны на сериал: ${serial.name}, к сожалению в моей базе еще нет озвучек, выбрать озвучку можно будет после того как увидете первое уведомление о ней`, opts)
          }
        } else {
          opts = {
            reply_markup: JSON.stringify({ remove_keyboard: true })
          }

          self.logger.info(`ADD: Подписали бесплатного пользователя: ${user.id} на сериал: ${serial.name}`)
          self.sendMessage(user, `Теперь вы подписаны на сериал: ${serial.name}`, opts)
        }
      })
      .catch((error) => {
        if (error instanceof UserNotFoundError) {
          self.sendMessage({ id: msg.chat.id }, UserNotExist)
        } else if (error instanceof LimitExceedError) {
          self.sendMessage(user, FreeSubscribe)
        } else if (error instanceof SerialNotFoundError) {
          self.logger.warn(`Не удалось найти сериал ${serialName} для пользователя ${msg.chat.id}`)
          self.sendMessage(user, `Не удалось однозначно идентифицировать ${serialName} для того что бы подписать вас, попробуйте еще раз или обратитесь за помощью в группу`)
        } else {
          self.logger.error(`Add: Неизвестная ошибка при добавление подписки ${serialName} для пользователя ${msg.chat.id}`)
          self.errorBot.sendMessage(`Add: Для ${msg.chat.id} ошибка ${error}`)
        }
      })
  }

  removeUserSerials (msg, match) {
    const self = this
    const serialName = escapeRegexp(match[1])
    let user
    let serial
    let subscription

    self.logger.warn(`Отписываем пользователя ${msg.chat.id} от ${serialName}`)

    self.trackIncoming({ text: match, userId: msg.chat.id })

    self._getSubscription(msg.chat.id, serialName)
      .then(([subs, ser, us]) => {
        user = us
        serial = ser
        subscription = subs

        const index = subscription.fans.findIndex((element) => { return element.user.equals(user._id) })

        if (index === -1) {
          /* user not subscribed */
          throw new NotSubscribedError()
        }

        subscription.fans = subscription.fans.splice(index, 1)
        subscription.save()
      })
      .then(() => {
        user.subscriptions -= 1

        if (user.subscriptions < 0) {
          user.subscriptions = 0
        }

        user.save()

        const opts = {
          reply_markup: JSON.stringify({ remove_keyboard: true })
        }

        self.logger.info(`Remove: Отписали пользователя: ${user.id} от сериала: ${serial.name}`)
        self.sendMessage(user, `Вы отписались от сериала: ${serial.name}`, opts)
      })
      .catch((error) => {
        if (error instanceof UserNotFoundError) {
          self.sendMessage({ id: msg.chat.id }, UserNotExist)
        } else if (error instanceof NotSubscribedError) {
          self.sendMessage(user, 'Вы не подписаны на этот сериал!')
        } else if (error instanceof SerialNotFoundError) {
          self.logger.warn(`Не удалось найти сериал ${serialName} для пользователя ${msg.chat.id}`)
          self.sendMessage(user, `Не удалось однозначно идентифицировать ${serialName} для того что бы отписать вас, попробуйте еще раз или обратитесь за помощью в группу`)
        } else {
          self.logger.error(`Remove: Неизвестная ошибка при отписки пользователя ${msg.chat.id} от ${serialName}`)
          self.errorBot.sendMessage(`Remove: Для ${msg.chat.id} ошибка ${error}`)
        }
      })
  }

  _createContext (subscription) {
    /* TODO: create database doc with subscription._id reference */
  }

  /*
   * TODO:
   *  - add 'В озвучке' command
   *  - add 'В любой озвучке' command
   *  - add 'Достаточно озвучек' command
   */
}

module.exports = BotUI
