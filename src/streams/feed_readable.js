const log4js = require('log4js')
const request = require('request-promise-native')
const feedparser = require('feedparser-promised')
const uniqWith = require('lodash.uniqwith')
const PeriodicReadable = require('./periodic_readable')

log4js.configure({
  appenders: { FeedReadable: { type: 'stdout' } },
  categories: { default: { appenders: ['FeedReadable'], level: 'debug' } }
})

class FeedReadable extends PeriodicReadable {
  constructor (site, parser, timeout, loggin) {
    super(timeout)

    if (!site || !parser) {
      throw new Error('Feed must have site and parser')
    }

    this.loggin = loggin
    this.site = site
    this.parser = parser
    this.logger = log4js.getLogger('FeedReadable')
  }

  _fetch () {
    const stream = this

    if (stream.loggin) {
      const jar = request.jar()

      request
        .get({
          url: stream.loggin,
          followAllRedirects: true,
          jar: jar
        })
        .then((result) => {
          stream._parse(feedparser.parse({ url: stream.site, jar: jar }))
        })
    } else {
      stream._parse(feedparser.parse(stream.site))
    }
  }

  _uniq (items) {
    return uniqWith(items, (one, two) => {
      return one.name === two.name &&
         one.season === two.season &&
         one.series[0].num === two.series[0].num
    })
  }

  _parse (feed) {
    const stream = this

    feed
      .then((items) => {
        const promises = items
          .map(stream.parser)

        return Promise.all(promises)
      })
      .then((results) => {
        const announcements = results.filter((item) => item !== null)
        const news = stream._uniq(announcements)

        console.log(`Uniq ${news.length}`)

        news.forEach((item) => {
          stream.logger.info(`Вышла ${item.series[0].num} ${item.season} сериала ${item.name} в озвучке ${item.series[0].studio}`)

          /*
           * we did no handle backpressure because
           * it's not a problem for us
           */
          stream.push(item)
        })
      }) 
      .catch((error) => {
        stream.logger.error(error)
      })
  }
}

module.exports = FeedReadable
