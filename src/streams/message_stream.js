const { Writable } = require('stream')
const log4js = require('log4js')
const handleBlock = require('../handle_user_block')

log4js.configure({
  appenders: { messageStream: { type: 'stdout' } },
  categories: { default: { appenders: ['messageStream'], level: 'debug' } }
})

module.exports = class MessageStream extends Writable {
  constructor (options) {
    if (options !== undefined && options.highWaterMark !== undefined) {
      super({ objectMode: true, highWaterMark: options.highWaterMark })
    } else {
      super({ objectMode: true })
    }

    this.logger = log4js.getLogger('messageStream')
  }

  _write (chunk, encoding, callback) {
    const stream = this

    if (chunk.fan === undefined ||
        chunk.fan.id === undefined ||
        chunk.message === undefined ||
        chunk.bot === undefined) {
      this.logger.error(`Невозможно отправить сообщение, пользователь или сообщение отсутствуют`)
      return callback()
    }

    chunk.bot.sendMessage(chunk.fan.id, chunk.message)
      .then(() => callback())
      .catch((error) => {
        const res = error.toString().match(/Error: ETELEGRAM:\s(\d+)/)

        stream.logger.error(`Ошибка посылки сообщения пользователю: ${error}`)

        if (res !== null && res[1] === '403') {
          stream.logger.warn(`Бот был заблокирован пользователем: ${chunk.fan.id}`)

          chunk.fan.active = 0
          return chunk.fan.save()
            .then(() => {
              if (chunk.fan.clearSubscriptions !== undefined) {
                return chunk.fan.clearSubscriptions()
              } else {
                return
              }
            })
            .then(() => callback())
            .catch((error) => {
              stream.logger.error(`Бот был заблокирован пользователем и мы не смогли его сохранить: ${chunk.fan.id}: ${error}`)
              callback()
            })
        } else {
          callback()
        }
      })
  }
}
