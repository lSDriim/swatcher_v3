const log4js = require('log4js')
const { Transform } = require('stream')
const escapeStringRegexp = require('escape-string-regexp')
const Serial = require('../models/serial')
const parseSeasonvarPage = require('../parsers/parse_seasonvar')

/* TODO: change, use ErrorBot.sendMessage witout chat id */
if (!process.env.ERROR_BOT_CHAT_ID) {
  throw new Error('"ERROR_BOT_CHAT_ID" environment variable must be defined')
}

const CHAT_ID = process.env.ERROR_BOT_CHAT_ID

log4js.configure({
  appenders: { AnnouncementChecker: { type: 'stdout' } },
  categories: { default: { appenders: ['AnnouncementChecker'], level: 'debug' } }
})

class AnnouncementChecker extends Transform {
  constructor (errorbot, watermark) {
    if (watermark !== undefined) {
      super({ objectMode: true, highWaterMark: watermark })
    } else {
      super({ objectMode: true })
    }

    if (errorbot.sendMessage === undefined) {
      throw new Error('errorbot must have sendMessage function')
    }

    this.errorbot = errorbot
    this.logger = log4js.getLogger('AnnouncementChecker')
  }

  _seasonCheck (serial, seasonName) {
    return serial.season.some((season) => {
      return season.name === seasonName
    })
  }

  _next (data, serial, callback) {
    data.serial = serial
    /* no backpressure problem in next stream */
    this.push(data)

    callback()
  }

  _parseSerial (data, follow, callback) {
    const checker = this

    if (data.serial_url !== undefined) {
      parseSeasonvarPage(data.serial_url, data.name, follow)
        .then((serial) => {
          checker._next(data, serial, callback)
        }).catch((err) => {
          checker.logger.error(`Ошибка при попытки распарсить страницу сериала ${data.name}: ${err}`)

          callback()
        })
    } else {
      callback()
    }
  }

  _transform (data, encoding, callback) {
    const checker = this

    if (data.series === undefined || data.name === undefined) {
      this.logger.error(`Попытка проверить неподходящий объект ${data}`)
      callback()
      return
    }

    const regex = new RegExp(['^', escapeStringRegexp(data.name), '$'].join(''), 'i')

    if (data.season === undefined) {
      data.season = '1 сезон'
    }

    Serial.find()
      .or([{ name: regex }, { alias: regex }])
      .then((serials) => {
        /* we need to call callback or _parseSerial in the end */
        if (serials.length === 0) {
          const message = `Не удалось найти ${data.name} в базе данных... студия: ${data.series[0].studio}`

          checker.logger.warn(message) /* TODO: do we realy need this line? */
          checker.errorbot.sendMessage(CHAT_ID, message)

          checker._parseSerial(data, true, callback)
        } else if (serials.length > 1) {
          const message = `Было найдено ${serials.length} копий сериала ${data.name}, новость будет отфильтрована`

          checker.logger.error(message)
          checker.errorbot.sendMessage(CHAT_ID, message)

          callback()
        } else {
          if (!checker._seasonCheck(serials[0], data.season)) {
            /* so, new season */
            checker.logger.warn(`${data.season} сериала ${data.name} еще нет...`)

            if (data.serial_url === undefined) {
              /* no link to parse season info, we can live without season info */
              checker._next(data, serials[0], callback)
            } else {
              checker._parseSerial(data, false, callback)
            }
          } else {
            /* all good, go to the next stream */
            checker._next(data, serials[0], callback)
          }
        }
      })
      .catch((error) => {
        checker.logger.error(`Поиск сериала вернул ошибку: ${error}`)

        callback()
      })
  }
}

module.exports = AnnouncementChecker
