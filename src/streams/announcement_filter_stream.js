const log4js = require('log4js')
const { Transform } = require('stream')
const Announcement = require('../models/announcement')

log4js.configure({
  appenders: { AnnouncementFilter: { type: 'stdout' } },
  categories: { default: { appenders: ['AnnouncementFilter'], level: 'debug' } }
})

class AnnouncementFilter extends Transform {
  constructor (watermark) {
    if (watermark !== undefined) {
      super({ objectMode: true, highWaterMark: watermark })
    } else {
      super({ objectMode: true })
    }

    this.logger = log4js.getLogger('AnnouncementFilter')
  }

  _transform (data, encoding, callback) {
    const filter = this

    if (data.serial === undefined ||
       data.series === undefined ||
       data.serial.name === undefined) {
      this.logger.error(`Попытка отфильтровать неподходящий объект ${JSON.stringify(data)}`)
      callback()
      return
    }

    if (!data.season) {
      /* First season often doesn't have season string */
      data.season = '1 сезон'
    }

    for (let i = 0; i < data.series.length; i++) {
      const query = Announcement.find()

      query.where('season', data.season)
      query.where('name', data.serial.name)

      query.where('series.num').equals(data.series[i].num)
      query.where('series.studio').equals(data.series[i].studio)

      query.exec()
        .then(existingAnnouncement => {
          if (existingAnnouncement.length === 0) {
            filter.logger.info(`Новая ${data.series[0].num} ${data.season} сериала ${data.serial.name} в озвучке ${data.series[0].studio}`)

            if (!filter.push(data)) {
              filter.logger.warn('AnnounceFilter: Очередь полна...')
              /* FIXME: нужно остановить обработку данных пока не случится событие drain */
            }
          }

          callback()
        })
        .catch(error => {
          filter.logger.error(`Поиск сериала вернул ошибку: ${error}`)
          callback()
        })
    }
  }
}

module.exports = AnnouncementFilter
