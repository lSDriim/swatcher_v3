const log4js = require('log4js')
const { Transform } = require('stream')

log4js.configure({
  appenders: { AnnouncementVoiceover: { type: 'stdout' } },
  categories: { default: { appenders: ['AnnouncementVoiceover'], level: 'debug' } }
})

class AnnouncementVoiceover extends Transform {
  constructor () {
    super({ objectMode: true })

    this.logger = log4js.getLogger('AnnouncementVoiceover')
  }

  _next (data, callback) {
    this.push(data)
    callback()
  }

  _transform (data, encoding, callback) {
    const voiceover = this

    if (data.series === undefined || data.serial === undefined) {
      this.logger.error(`Попытка работы с неверными данными ${data}`)
      callback()
      return
    }

    data.series.forEach((series) => {
      if (series.studio === '') {
        voiceover._next(data, callback)
        return /* no voice over */
      }

      if (data.serial.voice_over === null || data.serial.voice_over === undefined) {
        data.serial.voice_over = []
      }

      if (data.serial.voice_over.indexOf(series.studio) === -1) {
        data.serial.voice_over.push(series.studio)
        voiceover.logger.info(`Добавляем озвучку ${series.studio} сериалу ${data.serial.name}`)

        /* result not so important */
        data.serial.save()
          .then(() => {
            voiceover._next(data, callback)
          })
          .catch((error) => {
            voiceover.logger.error(`Добавление озвучки сериала вернуло ошибку: ${error}`)
            voiceover._next(data, callback)
          })
      } else {
        voiceover._next(data, callback)
      }
    })
  }
}

module.exports = AnnouncementVoiceover
