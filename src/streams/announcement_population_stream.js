const log4js = require('log4js')
const { Transform } = require('stream')

log4js.configure({
  appenders: { AnnouncementPopulation: { type: 'stdout' } },
  categories: { default: { appenders: ['AnnouncementPopulation'], level: 'debug' } }
})

class AnnouncementPopulation extends Transform {
  constructor (model, bot) {
    super({ objectMode: true })

    if (model === undefined) {
      throw new Error('model must be set')
    }

    if (bot === undefined) {
      throw new Error('bot must be set')
    }

    this.logger = log4js.getLogger('AnnouncementPopulation')
    this.bot = bot
    this.model = model
  }

  _subscribed (subsVoices, series) {
    const result = series.filter((n) => {
      return subsVoices.indexOf(n.studio) !== -1
    })

    return result.length > 0
  }

  _message (data) {
    let message = ''
    const reg = /(субтитры)/i

    for (var i = 0; i < data.series.length; i++) {
      message += `${data.series[i].num} `

      if (data.season !== undefined && data.season !== '') {
        message += `${data.season}а `
      } else {
        message += `1 сезона `
      }

      message += `${data.serial.name} `
      if (data.series[i].studio !== '') {
        if (reg.exec(data.series[i].studio)) {
          message += `с субтитрами\n`
        } else {
          message += `в озвучке ${data.series[i].studio}\n`
        }
      } else {
        message += `\n`
      }
    }

    return message
  }

  _send (fans, series, message, callback) {
    const pop = this
    let i

    if (!Array.isArray(fans) ||
        !Array.isArray(series) || message === undefined) {
      pop.logger.error(`Неправильный формат дынных`)
      callback()
      return
    }

    if (fans.length === 0) {
      callback()
      return
    }

    for (i = 0; i < fans.length; i++) {
      const fan = fans[i]

      if (fan.user.active !== undefined && fan.user.active === 0) {
        pop.logger.error(`Фанат не активен: ${fan.user.id}`)
        continue
      }

      if (fan.subs_voice.length !== 0) {
        if (pop._subscribed(fan.subs_voice, series) !== true) {
          /* user not subscribed to this voice over */
          continue
        }
      }

      if (!pop.push({ fan: fan.user, message: message, bot: pop.bot })) {
        /* backpressure */
        pop.once('drain', () => {
          pop.logger.warn('Drain event')
          pop._send(fans.slice(i), series, message, callback)
        })

        break
      }
    }

    if (i === (fans.length)) {
      /* all done in other case callback we be called on drain event */
      callback()
    }
  }

  _transform (data, encoding, callback) {
    const pop = this

    if (data.series === undefined ||
       data.season === undefined ||
       data.serial === undefined) {
      pop.logger.error(`Попытка обработать новость без необходимых данных`)
      pop.logger.error(data)
      callback()
      return
    }

    pop.model
      .findOne({ serial: data.serial._id })
      .populate('fans.user')
      .then((subs) => {
        const message = pop._message(data)

        if (subs === null) {
          pop.logger.info(`У сериала ${data.serial.name} нет подписчиков`)
          callback()
          return
        }

        pop._send(subs.fans, data.series, message, callback)
      })
      .catch((error) => {
        pop.logger.error(`Поиск подписок вернул ошибку: ${error}`)
        callback()
      })
  }
}

module.exports = AnnouncementPopulation
