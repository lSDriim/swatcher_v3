const log4js = require('log4js')
const { Transform } = require('stream')
const Announcement = require('../models/announcement')

log4js.configure({
  appenders: { AnnouncementSaver: { type: 'stdout' } },
  categories: { default: { appenders: ['AnnouncementSaver'], level: 'debug' } }
})

class AnnouncementSaver extends Transform {
  constructor () {
    super({ objectMode: true })

    this.logger = log4js.getLogger('AnnouncementSaver')
  }

  _transform (data, encoding, callback) {
    if (data.date === undefined ||
       data.series === undefined ||
       data.name === undefined ||
       data.season === undefined ||
       data.serial === undefined) {
      this.logger.error(`Попытка сохранить новость без сериала`)
      this.logger.error(data)
      callback()
      return
    }

    const saver = this
    const announcement = new Announcement({
      date: data.date,
      season: data.season,
      series: data.series,
      name: data.serial.name,
      serial: data.serial.id
    })

    announcement.save()
      .then(() => {
        saver.push(data)

        callback()
      })
      .catch((error) => {
        saver.logger.error(`Запись анонса вернула ошибку: ${error}`)
        callback()
      })
  }
}

module.exports = AnnouncementSaver
