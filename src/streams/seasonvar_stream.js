const log4js = require('log4js')
const cheerio = require('cheerio')
const request = require('request-promise-native')
const PeriodicReadable = require('./periodic_readable')

log4js.configure({
  appenders: { SeasonvarStream: { type: 'stdout' } },
  categories: { default: { appenders: ['SeasonvarStream'], level: 'debug' } }
})

const SeasonvarSite = 'http://seasonvar.ru'

function parseSerialName ($) {
  let regexp = /(.+) \/ (.+)/
  let name

  name = regexp.exec($.children('div[class="news-w"]').children('div[class="news_n"]').text().replace(/\r?\n|\r/g, '').trim())

  if (name !== null) {
    return name[1]
  }

  return $.children('div[class="news-w"]').children('div[class="news_n"]').text().replace(/\r?\n|\r/g, '').trim()
}

function cleanText (text) {
  let reg = /\((.+)\)/

  return reg.exec(text)
}

function parseNumSeries (number) {
  const couple = number.split(',')
  const series = []

  if (!couple && couple.length > 1 && couple[0].length > 7) {
    /* couple of studios */
    let numbers = couple[0].substring(0, couple[0].indexOf('(')).trim()
    series.push({
      num: numbers,
      studio: couple[0].substring(couple[0].indexOf('(') + 1)
    })

    series.push({
      num: numbers,
      studio: couple[1].substring(0, couple[1].indexOf(')')).trim()
    })
  } else {
    let res = cleanText(number)

    if (res !== null) {
      series.push({
        num: number.substring(0, number.indexOf('(')).trim(),
        studio: res[1]
      })
    } else {
      series.push({
        num: number,
        studio: ''
      })
    }
  }

  return series
}

function parseSeason (text) {
  let season = ''

  var result = cleanText(text)

  if (result !== null) {
    season = result[1]
  }

  return season
}

class SeasonvarStream extends PeriodicReadable {
  constructor (timeout) {
    super(timeout)
    this.logger = log4js.getLogger('SeasonvarStream')
  }

  _fetch () {
    const stream = this

    const options = {
      uri: SeasonvarSite,
      transform: (body) => {
        return cheerio.load(body)
      }
    }

    request(options)
      .then(($) => {
        let days = $('div[class="news"]')
        let count = days.length

        if (days.length > 3) {
          count = 3
        }

        for (let i = 0; i < count; i++) {
          const args = $(days[i])
            .find('div[class="news-head"]')
            .text().trim().split('.')

          const date = `${args[1]}.${args[0]}.${args[2]}`
          const news = $(days[i]).find('a')

          for (let j = 0; j < news.length; j++) {
            const serial = {}

            serial.date = date
            /* serial name */
            serial.name = parseSerialName($(news[j]))
            /* url */
            serial.serial_url = SeasonvarSite + $(news[j]).attr('href')
            /* series */
            serial.series = parseNumSeries($(news[j]).children('div[class="news-w"]').children('span[class="news_s"]').text())

            $(news[j])
              .children('div[class="news-w"]')
              .children('div[class="news_n"]')
              .remove()

            $(news[j])
              .children('div[class="news-w"]')
              .children('span')
              .remove()

            /* season */
            serial.season = parseSeason($(news[j]).children('div[class="news-w"]').text())

            /* no need to handle backpressure */
            if (!stream.push(serial)) {
              stream.logger.error('Очередь полна...')
            }
          }
        }
      })
      .catch((error) => {
        stream.logger.error(error)
      })
  }
}

module.exports = SeasonvarStream
