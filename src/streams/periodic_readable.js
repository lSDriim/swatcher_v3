const { Readable } = require('stream')

const HIGH = 100000
const TIMEOUT = 10000

class PeriodicReadable extends Readable {
  constructor (timeout) {
    super({ objectMode: true, highWaterMark: HIGH })

    if (timeout) {
      this.timeout = timeout
    } else {
      this.timeout = TIMEOUT
    }
  }

  _fetch () {
    /* will be overrided */
  }

  _read (size) {
    /* Empty function, all work done by other functions */
  }

  pipe (destination, options) {
    const stream = this

    this.interval = setInterval(function () {
      stream._fetch()
    },
    this.timeout)

    return super.pipe(destination, options)
  }

  unpipe (destination) {
    clearInterval(this.interval)

    return super.unpipe(destination)
  }
}

module.exports = PeriodicReadable
