const { Schema, model } = require('mongoose')

const serialSchema = new Schema({
  name: String,
  alias: [String], /* FIXME: optimize searching by this param too */
  genre: [String],
  country: [String],
  director: [String],
  voice_over: [String],
  season: [{
    name: String,
    desc: String,
    img: String,
    url: String,
    starts: Number, /* FIXME: can't be less than 1950 */
    actors: [String]
  }]
})

module.exports = model('serials', serialSchema)
