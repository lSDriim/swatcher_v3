const { model } = require('mongoose')
const clearSubs = require('../clear_subs')
const UserSchema = require('../schemas/user_schema')

UserSchema.methods.clearSubscriptions = function () {
  const user = this
  const subs = model('subscriptions_tg')

  return clearSubs(user, subs)
}

module.exports = model('users_tg', UserSchema)
