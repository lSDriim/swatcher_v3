const { Schema, model } = require('mongoose')

const specialSchema = new Schema({
  name: String,
  studio: String,
  alias: String
})

module.exports = model('special', specialSchema)
