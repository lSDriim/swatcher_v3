const { Schema, model } = require('mongoose')

/* FIXME: use compound uniq index */

const announceSchema = new Schema({
  name: String,
  date: Date,
  serial: { type: Schema.Types.ObjectId, ref: 'serials' },
  season: String,
  series: [{
    num: String,
    studio: String
  }]
})

module.exports = model('announcement', announceSchema)
