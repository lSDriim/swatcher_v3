const { Schema, model } = require('mongoose')
const UserSubSchema = require('../schemas/subscription_schema_tg')

const SubscriptionSchema = new Schema({
  serial: {
    type: Schema.Types.ObjectId,
    ref: 'serials',
    unique: true
  },
  fans: [UserSubSchema]
})

module.exports = model('subscriptions_tg', SubscriptionSchema)
