const checkSpecialCases = require('../special_cases')
const studio = 'Kuraj-Bambey'

module.exports = (item) => {
  return new Promise((resolve, reject) => {
    const result = item.title.match(/(.+)\s+(\d+)\s+сезон\s+(\d+)\s+серия/)

    if (!result) {
      resolve(null)
    }

    checkSpecialCases(result[1].trim(), studio)
      .then((name) => {
        resolve({
          name: name,
          date: item.date,
          season: Number(result[2].trim()) + ' сезон',
          series: [{
            num: Number(result[3].trim()) + ' серия',
            studio: studio
          }]
        })
      })
  })
}
