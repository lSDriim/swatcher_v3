const checkSpecialCases = require('../special_cases')
const regex = /\/\s?(.+)(Сезон|Сезон:)\s(\d)(\s?\/\s?|\s+)(Эпизод|Эпизоды|Серии|Серии:)\s?\d-(\d+)(.+)(Кубик в Кубе)\)$/
const studio = 'кубик в кубе'

module.exports = (item) => {
  return new Promise((resolve, reject) => {
    const result = item.title.match(regex)

    if (!result) {
      resolve(null)
    }

    let name = result[1].trim()
    if (name.indexOf('/') !== -1) {
      name = name.slice(0, name.indexOf('/'))
      name = name.trim()
    }

    if (name.indexOf('|') !== -1) {
      name = name.slice(0, name.indexOf('|'))
      name = name.trim()
    }

    checkSpecialCases(name, studio)
      .then((name) => {
        resolve({
          name: name,
          date: item.date,
          season: Number(result[3].trim()) + ' сезон',
          series: [{
            num: Number(result[6].trim()) + ' серия',
            studio: studio
          }]
        })
      })
  })
}
