const request = require('request-promise-native')
const cheerio = require('cheerio')
const log4js = require('log4js')
const escapeRegexp = require('escape-string-regexp')
const Serial = require('../models/serial')

log4js.configure({
  appenders: { ParseSeasonvar: { type: 'stdout' } },
  categories: { default: { appenders: ['ParseSeasonvar'], level: 'debug' } }
})

const S_INFO_DIV = 'div[class="pgs-sinfo-info"]'
const S_INFO_LIST = 'div[class="pgs-sinfo_list"]'
const S_INFO_ACTOR = 'div[class="pgs-sinfo-actor"]'

const logger = log4js.getLogger('ParseSeasonvar')

function getActors ($) {
  const actors = []

  $(S_INFO_ACTOR)
    .children('a')
    .each(function (i, elem) {
      actors.push($(this).children('span').text().trim())
    })

  return actors
}

function getAttrList ($) {
  return $(S_INFO_DIV).children(S_INFO_LIST)
}

function parseStringArray (str) {
  if (str !== undefined) {
    const arr = str.trim().split(',')

    for (let i = 0; i < arr.length; i++) {
      arr[i] = arr[i].trim()
    }

    return arr
  } else {
    return []
  }
};

function getAlias ($) {
  let alias = []

  getAttrList($)
    .first()
    .children('span')
    .each(function (val) {
      alias.push($(this).text().trim())
    })

  return alias
}

function getGenre ($) {
  return parseStringArray(getAttrList($)
    .eq(1)
    .children('span')
    .eq(0)
    .text())
}

function getCountry ($) {
  /* use last span from array */
  return parseStringArray(getAttrList($)
    .eq(1)
    .children('span')
    .eq(-1)
    .text())
}

function getDirector ($) {
  let director = []

  getAttrList($)
    .eq(2)
    .children('div')
    .children('span')
    .each(function (val) {
      director.push($(this).text().trim())
    })

  return director
}

function getStarts ($) {
  let starts = 0

  starts = parseInt(getAttrList($)
    .eq(2)
    .children('span')
    .eq(0)
    .text())

  /* TODO: check result */
  return starts
}

function getSeasonName ($, name, origin) {
  let retval = '1 сезон'
  let seasonName = $('h1[class="pgs-sinfo-title"]').text().trim()
  let nameEscaped = escapeRegexp(name)
  let originEscaped = escapeRegexp(origin)
  let nameRegexp = new RegExp(`Сериал\\s+(${nameEscaped})\\/(${originEscaped})\\s+(.+)\\s+онлайн`)
  let result = nameRegexp.exec(seasonName)

  if (result == null) {
    /* Russian serial */
    nameRegexp = new RegExp(`Сериал\\s+(${nameEscaped})\\s+(.+)\\s+онлайн`)
    result = nameRegexp.exec(seasonName)

    if (result !== null) {
      retval = result[2]
    }
  } else {
    retval = result[3]
  }

  return retval
}

function parseHTML ($, url, name) {
  /* return value must be ready to safe db object */
  const info = new Serial({
    alias: [],
    genre: [],
    country: [],
    director: [],
    season: [{}]
  })

  /* Serial name */
  info.name = name
  /* Serial alias */
  info.alias = getAlias($)
  /* Serial genre */
  info.genre = getGenre($)
  /* Serial country */
  info.country = getCountry($)
  /* Serial director */
  info.director = getDirector($)
  /* Serial season url */
  info.season[0].url = url
  /* Serial season poster url */
  info.season[0].img = $('.poster').children('img').attr('src')
  /* Serial season description */
  info.season[0].desc = $('div[class="pgs-sinfo-info"]').children('p').text()
  /* Serial season actors */
  info.season[0].actors = getActors($)
  /* Serial season starts */
  info.season[0].starts = getStarts($)
  /* Serial season name */
  info.season[0].name = getSeasonName($, info.name, info.alias[0])

  return info
}

function checkDatabase (serial) {
  return new Promise((resolve, reject) => {
    /* TODO: add more params to search request */
    Serial.findOne({ name: serial.name, country: serial.country })
      .then((result) => {
        if (result == null) {
          logger.info(`В базе данных нет сериала ${serial.name}, создаем...`)
          resolve(serial)
        } else {
          const duplicate = result.season.some((elem) => {
            return elem.url === serial.season[0].url
          })

          if (duplicate === false) {
            logger.info(`Сериал ${serial.name} есть, а ${serial.season[0].name} нет. Добавляем...`)
            result.season.push(serial.season[0])
          }

          resolve(result)
        }
      })
      .catch((error) => {
        reject(error)
      })
  })
}

function parseAllSeasons (serial, $) {
  return new Promise((resolve, reject) => {
    const promises = []

    $('h2').each(function (i, elem) {
      const url = 'http://seasonvar.ru' + $(this).children('a').first().attr('href')

      promises.push(parseSeasonvarPage(url, serial.name, false))
    })

    Promise.all(promises)
      .then(() => Serial.findOne({ _id: serial._id }))
      .then((retval) => resolve(retval))
  })
}

function parseSeasonvarPage (url, name, follow) {
  return new Promise((resolve, reject) => {
    let page = null

    const options = {
      uri: url,
      transform: (body) => {
        return cheerio.load(body)
      }
    }

    request(options)
      .then($ => {
        /* parsing page */
        const info = parseHTML($, url, name)

        page = $

        return checkDatabase(info)
      }).then((serial) => {
        /*
         * in case of existing and not changed serial, save will not do anything
         * but we need save, because checkDatabase can add new season.
         */
        return serial.save()
      }).then((serial) => {
        if (follow) {
          resolve(parseAllSeasons(serial, page))
        } else {
          resolve(serial)
        }
      }).catch((err) => {
        logger.error(`Попытка спарсить страницу ${url} привела к ошибке: ${err}`)
        reject(err)
      })
  })
}

module.exports = parseSeasonvarPage
