const checkSpecialCases = require('../special_cases')
const studio = 'BaibaKo'

module.exports = (item) => {
  return new Promise((resolve, reject) => {
    const result = item.title.match(/\/(.+)\s\/s(\d+)e(\S+)/)

    if (!result) {
      resolve(null)
    }

    let series = result[3].trim()
    let num = result[3].trim().match(/(\d+)-(\d+)/)

    if (num) {
      series = num[2]
    }

    checkSpecialCases(result[1].trim(), studio)
      .then((name) => {
        resolve({
          name: name,
          date: item.date,
          season: Number(result[2].trim()) + ' сезон',
          series: [{
            num: Number(series) + ' серия',
            studio: studio
          }]
        })
      })
  })
}
