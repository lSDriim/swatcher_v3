const checkSpecialCases = require('../special_cases')
const studio = 'ColdFilm'

module.exports = (item) => {
  return new Promise((resolve, reject) => {
    const slash = /.+\/\s(.+)\s(\d+)\sсезон\s+(\d+)\s+серия\s+/
    const regexp = /(.+)\s(\d+)\sсезон\s+(\d+)\s+серия\s+/
    let result = item.title.match(slash)

    if (!result) {
      /* Some titles has slashes */
      result = item.title.match(regexp)

      if (!result) {
        resolve(null)
      }
    }

    if (item.summary.match(/(Трейлер)/i)) {
      /* Pass trailers */
      resolve(null)
    }

    checkSpecialCases(result[1].trim(), studio)
      .then((name) => {
        resolve({
          name: name,
          date: item.date,
          season: Number(result[2].trim()) + ' сезон',
          series: [{
            num: Number(result[3].trim()) + ' серия',
            studio: studio
          }]
        })
      })
  })
}
