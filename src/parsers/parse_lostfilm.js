const checkSpecialCases = require('../special_cases')
const studio = 'LostFilm'

module.exports = (item) => {
  return new Promise((resolve, reject) => {
    const result = item.title.match(/(.+)\s\((.+)\)..+\(S(\d+)E(\d+)\)/)

    if (!result) {
      resolve(null)
    }

    checkSpecialCases(result[1].trim(), studio)
      .then((name) => {
        resolve({
          name: name,
          date: item.date,
          season: Number(result[3].trim()) + ' сезон',
          series: [{
            num: Number(result[4].trim()) + ' серия',
            studio: studio
          }]
        })
      })
  })
}
