const log4js = require('log4js')

log4js.configure({
  appenders: { userBlock: { type: 'stdout' } },
  categories: { default: { appenders: ['userBlock'], level: 'debug' } }
})

const logger = log4js.getLogger('userBlock')

module.exports = (fan, error, callback) => {
  return new Promise(function(resolve, reject) {
    const res = error.toString().match(/Error: ETELEGRAM:\s(\d+)/)

    logger.error(`Ошибка посылки сообщения пользователю: ${error}`)

    if (res !== null && res[1] === '403') {
      logger.warn(`Бот был заблокирован пользователем: ${fan.id}`)

      fan.active = 0
      return fan.save()
        .then(() => {
          if (!fan.clearSubscriptions) {
            return fan.clearSubscriptions()
          } else {
            return
          }
        })
        .then(() => callback())
        .catch((error) => {
          logger.error(`Бот был заблокирован пользователем и мы не смогли его сохранить: ${fan.id}: ${error}`)
          callback()
        })
    } else {
      callback()
    }
  })
}
