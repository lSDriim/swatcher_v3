const { Schema } = require('mongoose')

const UserSchema = new Schema({
  id: Number, /* FIXME: check number boundarys for each bot and eval this number, must be uniq */
  name: String,
  username: String,
  active: Number, /* not active means user blocked bot or somethings else */
  subscriptions: Number, /* number of subscriptions user have */
  payed: Number
})

module.exports = UserSchema
