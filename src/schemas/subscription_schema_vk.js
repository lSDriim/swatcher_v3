const { Schema } = require('mongoose')

module.exports = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'users_vk' },
  subs_voice: [String]
})
