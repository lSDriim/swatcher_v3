const Special = require('./models/special_case.js')

module.exports = (name, studio) => {
  return Special
    .findOne({ name, studio })
    .then((result) => {
      if (result) {
        return result.alias
      } else {
        return name
      }
    })
}
