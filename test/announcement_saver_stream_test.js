const { ReadableMock, WritableMock } = require('stream-mock')
const { expect } = require('chai')
const AnnounceSaver = require('../src/streams/announcement_saver_stream')
const Announcement = require('../src/models/announcement')
const parseSeasonvarPage = require('../src/parsers/parse_seasonvar')

describe('Checking saver stream', () => {
  it('should save announcement to database', (done) => {
    const url = 'http://seasonvar.ru/serial-15557--Igra_prestoloB-----07----sezon.html'
    const name = 'Игра престолов'
    const season = '3 сезон'
    const writer = new WritableMock({ objectMode: true })
    const filter = new AnnounceSaver()

    parseSeasonvarPage(url, name, false)
      .then((serial) => {
        const announcement = {
          date: '2017-11-23 23:57:13.000',
          name: serial.name,
          serial: serial,
          season: season,
          series: [{
            num: '7 серия',
            studio: 'ColdFilm'
          }]
        }

        const reader = new ReadableMock([announcement], { objectMode: true })

        reader.pipe(filter).pipe(writer)
      })

    writer.on('finish', () => {
      expect(writer.data.length).to.be.equal(1)

      Announcement.find({ name: name, season: season })
        .then((announcements) => {
          expect(announcements.length).to.be.equal(1)

          done()
        })
    })
  })
})
