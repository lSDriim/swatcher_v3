const mongoose = require('mongoose')

const opts = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
}

before((done) => {
  mongoose.connect('mongodb://localhost:27017/swatcher_test', opts)
  mongoose.connection
    .once('open', () => { done() })
    .on('error', (error) => { console.warn('Warning', error) })
})

beforeEach((done) => {
  const { announcements, serials, users_tgs, subscriptions_tgs, specials } = mongoose.connection.collections;
  announcements.drop(() => {
    serials.drop(() => {
      users_tgs.drop(() => {
        subscriptions_tgs.drop(() => {
          specials.drop(() => done())
        })
      })
    })
  })
})

after((done) => {
  mongoose.connection.close(() => done())
})
