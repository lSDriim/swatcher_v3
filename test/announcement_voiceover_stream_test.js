const { ReadableMock, WritableMock } = require('stream-mock')
const { expect } = require('chai')
const AnnouncementVoiceover = require('../src/streams/announcement_voiceover_stream')
const Serial = require('../src/models/serial')
const parseSeasonvarPage = require('../src/parsers/parse_seasonvar')

describe('Checking voiceover stream', () => {
  const url = 'http://seasonvar.ru/serial-15557--Igra_prestoloB-----07----sezon.html'
  const name = 'Игра престолов'
  const season = '7 сезон'
  let writer
  let filter
  let serial

  beforeEach((done) => {
    writer = new WritableMock({ objectMode: true })
    filter = new AnnouncementVoiceover()

    parseSeasonvarPage(url, name, false)
      .then((result) => {
        serial = result

        done()
      })
  })

  it('should add voiceover on a first met', (done) => {
    const announcement = {
      date: '2017-11-23 23:57:13.000',
      name: serial.name,
      serial: serial,
      season: season,
      series: [{
        num: '1 серия',
        studio: 'ColdFilm'
      }]
    }

    const reader = new ReadableMock([announcement], { objectMode: true })
    reader.pipe(filter).pipe(writer)

    writer.on('finish', () => {
      expect(writer.data.length).to.be.equal(1)

      Serial.findOne({ name: name })
        .then((result) => {
          expect(result.voice_over.length).to.be.equal(1)

          done()
        })
    })
  })

  it('should not add voiceover if it already exists', (done) => {
    const announcement = [{
      date: '2017-11-23 23:57:13.000',
      name: serial.name,
      serial: serial,
      season: season,
      series: [{
        num: '2 серия',
        studio: 'ColdFilm'
      }]
    },
    {
      date: '2017-11-23 23:57:13.000',
      name: serial.name,
      serial: serial,
      season: season,
      series: [{
        num: '1 серия',
        studio: 'ColdFilm'
      }]
    }]

    const reader = new ReadableMock(announcement, { objectMode: true })
    reader.pipe(filter).pipe(writer)

    writer.on('finish', () => {
      expect(writer.data.length).to.be.equal(2)

      Serial.findOne({ name: name })
        .then((result) => {
          expect(result.voice_over.length).to.be.equal(1)

          done()
        })
    })
  })
})
