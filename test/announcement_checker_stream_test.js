const { ReadableMock, WritableMock } = require('stream-mock')
const { expect } = require('chai')
const parseSeasonvarPage = require('../src/parsers/parse_seasonvar')

/* setting env variable before loading module */
process.env.ERROR_BOT_CHAT_ID = 'test'

const AnnounceChecker = require('../src/streams/announcement_checker_stream')

describe('Checking announcement checker', () => {
  let errbot
  let announcement
  let writer
  let transform
  let url
  let name
  let seasonUrl

  beforeEach(() => {
    name = 'Мистер Робот'

    errbot = {
      sendMessage: function (chat, message) {
        console.log(`Ошибка: ${message}`)
      }
    }

    announcement = [{
      date: '2017-11-23 23:57:13.000',
      name: name,
      season: '3 сезон',
      series: [{
        num: '7 серия',
        studio: 'ColdFilm'
      }]
    }]

    writer = new WritableMock({ objectMode: true })
    transform = new AnnounceChecker(errbot)

    url = 'http://seasonvar.ru/serial-11787-Mister_Robot-1-sezon.html'
    seasonUrl = 'http://seasonvar.ru/serial-16482-Mister_Robot-3-sezon.html'
  })

  it('must filter out announcement if there no serial in database and no serial_url', (done) => {
    const reader = new ReadableMock(announcement, { objectMode: true })

    reader.pipe(transform).pipe(writer)

    writer.on('finish', () => {
      expect(writer.data.length).to.equal(0)

      done()
    })
  })

  it('must NOT filter out announcement if there no serial in database, but has serial_url', (done) => {
    announcement[0].serial_url = seasonUrl

    const reader = new ReadableMock(announcement, { objectMode: true })

    reader.pipe(transform).pipe(writer)

    writer.on('finish', () => {
      expect(writer.data.length).to.equal(1)

      done()
    })
  })

  it('must NOT filter out announcement if there no season info and no serial_url', (done) => {
    parseSeasonvarPage(url, name, false)
      .then(() => {
        const reader = new ReadableMock(announcement, { objectMode: true })
        reader.pipe(transform).pipe(writer)
      })

    writer.on('finish', () => {
      expect(writer.data.length).to.equal(1)

      done()
    })
  })

  it('must NOT filter out announcement if there no season info and create season if had serial_url', (done) => {
    announcement[0].serial_url = seasonUrl

    parseSeasonvarPage(url, name, false)
      .then(() => {
        const reader = new ReadableMock(announcement, { objectMode: true })
        reader.pipe(transform).pipe(writer)
      })

    writer.on('finish', () => {
      expect(writer.data.length).to.equal(1)
      expect(writer.data[0].serial.season.length).to.equal(2)

      done()
    })
  })
})
