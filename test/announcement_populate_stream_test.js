/* eslint no-unused-expressions: 0 */
const { ReadableMock, WritableMock } = require('stream-mock')
const { expect } = require('chai')
const sinon = require('sinon')
const User = require('../src/models/user_tg')
const Subscription = require('../src/models/subscription_tg')
const parseSeasonvarPage = require('../src/parsers/parse_seasonvar')
const Announcement = require('../src/models/announcement')

const AnnouncementPopulation = require('../src/streams/announcement_population_stream')

describe('Checking anounce population stream _subscribed function', () => {
  const model = {}
  const bot = {}
  const filter = new AnnouncementPopulation(model, bot)
  const subs = ['Coldfilm', 'Lostfilm']

  it('should return "true" if series studio is in subsVoices', () => {
    const series = [{ num: '1 серия', studio: 'Coldfilm' }]
    const seriesMulti = [
      { num: '1 серия', studio: 'Coldfilm' },
      { num: '1 серия', studio: 'Baibako' }
    ]

    expect(filter._subscribed(subs, series)).to.be.true
    expect(filter._subscribed(subs, seriesMulti)).to.be.true
  })

  it('should return "false" if series studio is not in subsVoices', () => {
    const series = [{ num: '1 серия', studio: 'Baibako' }]
    const seriesMulti = [
      { num: '1 серия', studio: 'Test' },
      { num: '1 серия', studio: 'Baibako' }
    ]

    expect(filter._subscribed(subs, series)).to.be.false
    expect(filter._subscribed(subs, seriesMulti)).to.be.false
  })
})

describe('Checking anounce population stream _message function', () => {
  const model = {}
  const bot = {}
  const filter = new AnnouncementPopulation(model, bot)

  it('should handle normal message', () => {
    const data = {
      season: '1 сезон',
      serial: {
        name: 'Игра Престолов'
      },
      series: [{
        num: '1 серия',
        studio: 'Lostfilm'
      }]
    }

    const str = '1 серия 1 сезона Игра Престолов в озвучке Lostfilm\n'
    expect(filter._message(data)).to.be.equal(str)
  })

  it('should handle subtitle message', () => {
    const data = {
      season: '1 сезон',
      serial: {
        name: 'Игра Престолов'
      },
      series: [{
        num: '1 серия',
        studio: 'субтитры'
      }]
    }

    const str = '1 серия 1 сезона Игра Престолов с субтитрами\n'
    expect(filter._message(data)).to.be.equal(str)
  })

  it('should handle message without voice over', () => {
    const data = {
      season: '1 сезон',
      serial: {
        name: 'Игра Престолов'
      },
      series: [{
        num: '1 серия',
        studio: ''
      }]
    }

    const str = '1 серия 1 сезона Игра Престолов \n'
    expect(filter._message(data)).to.be.equal(str)
  })

  it('should handle message without season', () => {
    const data = {
      serial: {
        name: 'Игра Престолов'
      },
      series: [{
        num: '1 серия',
        studio: 'Lostfilm'
      }]
    }

    const str = '1 серия 1 сезона Игра Престолов в озвучке Lostfilm\n'
    expect(filter._message(data)).to.be.equal(str)
  })
})

describe('Checking anounce population stream _send function', () => {
  const model = {}
  const bot = {}
  const series = []
  const message = ''
  let filter
  let stub
  let spy

  beforeEach(() => {
    filter = new AnnouncementPopulation(model, bot)
    stub = sinon.stub(filter, 'push')
    spy = sinon.spy()
  })

  it('should handle wrong fans or empty array', () => {
    let fans = {}

    filter._send(fans, series, message, spy)

    expect(spy.called).to.be.true
    expect(stub.notCalled).to.be.true

    spy.resetHistory()
    stub.resetHistory()

    fans = []
    filter._send(fans, series, message, spy)

    expect(spy.called).to.be.true
    expect(stub.notCalled).to.be.true
  })

  it('should handle inactive user', () => {
    const fans = [{
      user: {
        active: 0,
        id: 0
      }
    }]

    filter._send(fans, series, message, spy)

    expect(spy.called).to.be.true
    expect(stub.notCalled).to.be.true
  })

  it('should handle backpressure problem', () => {
    const fans = [{
      subs_voice: [],
      user: {
        active: 1,
        id: 0
      }
    }]

    stub.returns(false)
    const once = sinon.stub(filter, 'once')

    filter._send(fans, series, message, spy)

    /* because we stubed function that call callback */
    expect(spy.notCalled).to.be.true
    expect(stub.called).to.be.true
    expect(once.called).to.be.true
  })

  it('should call callback if all okay', () => {
    const fans = [{
      subs_voice: [],
      user: {
        active: 1,
        id: 0
      }
    }]

    stub.returns(true)

    filter._send(fans, series, message, spy)

    expect(spy.called).to.be.true
    expect(stub.called).to.be.true
  })
})

describe('Checking anounce population stream _transform function', () => {
  it('should push 1 message to the next stream', (done) => {
    const bot = {}
    const url = 'http://seasonvar.ru/serial-15557--Igra_prestoloB-----07----sezon.html'
    const name = 'Игра престолов'
    let serial

    parseSeasonvarPage(url, name, false)
      .then((result) => {
        serial = result

        const user = new User({
          id: 1,
          name: 'Test',
          username: 'Test',
          active: 1
        })

        return user.save()
      })
      .then((user) => {
        const subs = new Subscription({
          serial: serial._id,
          fans: [{
            user: user._id,
            subs_voice: ['Coldfilm']
          }]
        })

        return subs.save()
      })
      .then((subs) => {
        const announce = new Announcement({
          date: '2017-11-23 23:57:13.000',
          season: '7 сезон',
          series: [{
            num: '1 серия',
            studio: 'Coldfilm'
          }],
          serial: serial
        })

        const reader = new ReadableMock([announce], { objectMode: true })
        const writer = new WritableMock({ objectMode: true })
        const filter = new AnnouncementPopulation(Subscription, bot)

        reader.pipe(filter).pipe(writer)

        writer.on('finish', () => {
          expect(writer.data.length).to.be.equal(1)
          done()
        })
      })
  })
})
