const Announcement = require('../src/models/announcement')
const Serials = require('../src/models/serial')
const AnnounceFilter = require('../src/streams/announcement_filter_stream')
const { ReadableMock, WritableMock } = require('stream-mock')
const { expect } = require('chai')

describe('Checking announcement filter', () => {
  let serial

  beforeEach((done) => {
    serial = new Serials({
      name: 'Мистер Робот'
    })

    serial.save()
      .then(() => {
        const announce = new Announcement({
          date: '2017-11-23 23:57:13.000',
          name: 'Мистер Робот',
          season: '3 сезон',
          series: {
            num: '7 серия',
            studio: 'ColdFilm'
          },
          serial: serial
        })

        return announce.save()
      })
      .then(() => done())
  })

  it('should filter out existing announcement', (done) => {
    const announce = new Announcement({
      date: '2017-11-23 23:57:13.000',
      season: '3 сезон',
      series: [{
        num: '7 серия',
        studio: 'ColdFilm'
      }],
      serial: serial
    })

    const reader = new ReadableMock([announce], { objectMode: true })
    const writer = new WritableMock({ objectMode: true })
    const filter = new AnnounceFilter()

    reader.pipe(filter).pipe(writer)

    writer.on('finish', () => {
      expect(writer.data.length).to.be.equal(0)

      done()
    })
  })

  it('should not filter out new announcement', (done) => {
    const announce = new Announcement({
      date: '2017-11-23 23:57:13.000',
      season: '3 сезон',
      series: [{
        num: '8 серия',
        studio: 'ColdFilm'
      }],
      serial: serial
    })

    const reader = new ReadableMock([announce], { objectMode: true })
    const writer = new WritableMock({ objectMode: true })
    const filter = new AnnounceFilter()

    reader.pipe(filter).pipe(writer)

    writer.on('finish', () => {
      expect(writer.data.length).to.be.equal(1)

      done()
    })
  })

  it('should handle announcement without season', (done) => {
    const announce = new Announcement({
      date: '2017-11-23 23:57:13.000',
      series: [{
        num: '9 серия',
        studio: 'ColdFilm'
      }],
      serial: serial
    })

    const reader = new ReadableMock([announce], { objectMode: true })
    const writer = new WritableMock({ objectMode: true })
    const filter = new AnnounceFilter()

    reader.pipe(filter).pipe(writer)

    writer.on('finish', () => {
      expect(writer.data.length).to.be.equal(1)

      done()
    })
  })
})
