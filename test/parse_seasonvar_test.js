const request = require('request-promise-native')
const chai = require('chai')
const assertArrays = require('chai-arrays')
const cheerio = require('cheerio')

const parseSeasonvarPage = require('../src/parsers/parse_seasonvar')

chai.use(assertArrays)
const expect = chai.expect

describe('Seasonvar parse and save serial', function () {
  /* request can be longer than 2000 ms */
  this.timeout(10000)

  it('must parse one season of Game of Thrones', (done) => {
    const url = 'http://seasonvar.ru/serial-15557--Igra_prestoloB-----07----sezon.html'
    const name = 'Игра престолов'

    parseSeasonvarPage(url, name, false)
      .then((serial) => {
        expect(serial.season.length).to.be.equal(1)
        expect(serial.name).to.be.equal('Игра престолов')
        expect(serial.season[0].img).to.equal('http://cdn.seasonvar.ru/oblojka/15557.jpg')
        expect(serial.season[0].actors).to.be.containingAllOf([
          'Питер Динклэйдж',
          'Лина Хиди',
          'Эмилия Кларк',
          'Кит Харингтон',
          'Николай Костер-Вальдау',
          'Мэйси Уильямс'
        ])
        expect(serial.alias).to.be.containingAllOf(['Game of Thrones'])
        expect(serial.genre).to.be.containingAllOf(['фэнтези', 'драмы'])
        expect(serial.country).to.be.containingAllOf(['США'])
        expect(serial.season[0].starts).to.equal(2017)
        expect(serial.director).to.be.containingAllOf(['Алан Тейлор', 'Брайан Кирк', 'Даниэль Минахан'])
        expect(serial.season[0].name).to.equal('7 сезон')

        done()
      })
  })

  it('must parse one season of Russian serial', (done) => {
    const url = 'http://seasonvar.ru/serial-17297-CHuzhoe_litco_2017.html'
    const name = 'Чужое лицо (2017)'

    parseSeasonvarPage(url, name, false)
      .then((serial) => {
        expect(serial.season.length).to.be.equal(1)
        expect(serial.name).to.be.containingAllOf(['Чужое лицо (2017)'])
        expect(serial.season[0].img).to.equal('http://cdn.seasonvar.ru/oblojka/17297.jpg')
        expect(serial.season[0].actors).to.be.containingAllOf(['Алексей Кравченко', 'Антон Момот', 'Валерий Соловьев', 'Аркадий Шароградский', 'Никита Барсуков', 'Ольга Павлюкова'])
        expect(serial.alias).to.be.containingAllOf(['Чужое лицо (2017)'])
        expect(serial.genre).to.be.containingAllOf(['детективы', 'криминальные', 'отечественные', 'приключения'])
        expect(serial.country).to.be.containingAllOf(['Россия'])
        expect(serial.season[0].starts).to.equal(2017)
        expect(serial.director).to.be.containingAllOf(['Иван Криворучко', 'Григорий Жихаревич'])
        expect(serial.season[0].name).to.equal('1 сезон')

        done()
      })
  })

  it('must add seasons', (done) => {
    const url = 'http://seasonvar.ru/serial-15557--Igra_prestoloB-----07----sezon.html'
    const name = 'Игра престолов'

    parseSeasonvarPage(url, name, false)
      .then((serial) => {
        expect(serial.season.length).to.be.equal(1)

        return parseSeasonvarPage('http://seasonvar.ru/serial-13535-Jon_Snow_voskresnet-000006-sezon.html', name, false)
      })
      .then((serial) => {
        expect(serial.season.length).to.be.equal(2)

        return parseSeasonvarPage('http://seasonvar.ru/serial-11242-Jon_Snow_umret--05--sezon.html', name, true)
      })
      .then((serial) => {
        expect(serial.season.length).to.be.equal(8)

        done()
      })
  })

  it('must parse all seasons', (done) => {
    const url = 'http://seasonvar.ru/serial-16482-Mister_Robot-3-sezon.html'
    const name = 'Мистер Робот'

    parseSeasonvarPage(url, name, true)
      .then((serial) => {
        expect(serial.season.length).to.be.equal(4)

        done()
      })
  })
})
