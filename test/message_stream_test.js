const { ReadableMock } = require('stream-mock')
const { expect } = require('chai')
const sinon = require('sinon')
const MessageStream = require('../src/streams/message_stream')

describe('Message stream operation', () => {
  let writer
  let chunkMock

  beforeEach(() => {
    writer = new MessageStream()

    chunkMock = {
      fan: {
        id: 1,
        active: 0,
        clearSubscriptions: sinon.stub().resolves()
      },
      message: 'test',
      bot: {}
    }
  })

  it('should not send message if chunk.fan.id doesn\'t exists', (done) => {
    const chunk = {
      message: 'Test',
      bot: {
        sendMessage: sinon.spy()
      }
    }

    const reader = new ReadableMock([chunk], { objectMode: true })
    reader.pipe(writer)

    writer.on('finish', () => {
      expect(chunk.bot.sendMessage.callCount).to.be.equal(0)

      done()
    })
  })

  it('should not send message if chunk.message doesn\'t exists', (done) => {
    const chunk = {
      fan: { id: 1 },
      bot: {
        sendMessage: sinon.spy()
      }
    }

    const reader = new ReadableMock([chunk], { objectMode: true })
    reader.pipe(writer)

    writer.on('finish', () => {
      expect(chunk.bot.sendMessage.callCount).to.be.equal(0)

      done()
    })
  })

  it('should call sendMessage if all exists, but not call clearSubscriptions', (done) => {
    chunkMock.bot.sendMessage = sinon.stub().resolves()

    const reader = new ReadableMock([chunkMock], { objectMode: true })
    reader.pipe(writer)

    writer.on('finish', () => {
      expect(chunkMock.bot.sendMessage.callCount).to.be.equal(1)
      expect(chunkMock.fan.clearSubscriptions.callCount).to.be.equal(0)

      done()
    })
  })

  it('should handle sendMessage reject', (done) => {
    chunkMock.bot.sendMessage = sinon.stub().rejects(new Error('ETELEGRAM: 404'))

    const reader = new ReadableMock([chunkMock], { objectMode: true })
    reader.pipe(writer)

    writer.on('finish', () => {
      expect(chunkMock.bot.sendMessage.callCount).to.be.equal(1)
      expect(chunkMock.fan.clearSubscriptions.callCount).to.be.equal(0)

      done()
    })
  })

  it('should handle sendMessage reject error 403', (done) => {
    chunkMock.bot.sendMessage = sinon.stub().rejects(new Error('ETELEGRAM: 403 '))
    chunkMock.fan.save = sinon.stub().resolves()

    const reader = new ReadableMock([chunkMock], { objectMode: true })
    reader.pipe(writer)

    writer.on('finish', () => {
      expect(chunkMock.bot.sendMessage.callCount).to.be.equal(1)
      expect(chunkMock.fan.clearSubscriptions.callCount).to.be.equal(1)
      expect(chunkMock.fan.save.callCount).to.be.equal(1)

      done()
    })
  })

  it('should handle sendMessage reject error 403(failed save)', (done) => {
    chunkMock.bot.sendMessage = sinon.stub().rejects(new Error('ETELEGRAM: 403 '))
    chunkMock.fan.save = sinon.stub().rejects()

    const reader = new ReadableMock([chunkMock], { objectMode: true })
    reader.pipe(writer)

    writer.on('finish', () => {
      expect(chunkMock.bot.sendMessage.callCount).to.be.equal(1)
      expect(chunkMock.fan.save.callCount).to.be.equal(1)
      expect(chunkMock.fan.clearSubscriptions.callCount).to.be.equal(0)

      done()
    })
  })

  it('should handle sendMessage reject error 403(failed clearSubscriptions)', (done) => {
    chunkMock.bot.sendMessage = sinon.stub().rejects(new Error('ETELEGRAM: 403 '))
    chunkMock.fan.save = sinon.stub().resolves()
    chunkMock.fan.clearSubscriptions = sinon.stub().rejects()

    const reader = new ReadableMock([chunkMock], { objectMode: true })
    reader.pipe(writer)

    writer.on('finish', () => {
      expect(chunkMock.bot.sendMessage.callCount).to.be.equal(1)
      expect(chunkMock.fan.save.callCount).to.be.equal(1)
      expect(chunkMock.fan.clearSubscriptions.callCount).to.be.equal(1)

      done()
    })
  })
})
