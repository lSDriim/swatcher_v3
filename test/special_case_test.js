const { expect } = require('chai')
const SpecialCases = require('../src/models/special_case')
const checkSpecial = require('../src/special_cases')

describe('Checking special case names function', () => {
  const name = 'Test Name'
  const studio = 'test studio'
  const alias = 'test alias'

  it('should return alias name if it in database', (done) => {
    const test = new SpecialCases({ name, studio, alias })

    test.save()
      .then(() => {
        return checkSpecial(name, studio)
      })
      .then((result) => {
        expect(result).to.be.equal(alias)
        done()
      })
  })

  it('should return name if it\'s not in database', (done) => {
    const test = new SpecialCases({ name, studio, alias })
    const another = 'Another Serial'

    test.save()
      .then(() => {
        return checkSpecial(another, studio)
      })
      .then((result) => {
        expect(result).to.be.equal(another)
        done()
      })
  })
})
