const { expect } = require('chai')
const sinon = require('sinon')
const mongoose = require('mongoose')

const Serial = require('../src/models/serial')
const UI = require('../src/swatcher_ui.js')
const UserNotExist = require('../src/text').user
const FreeSubscribe = require('../src/text').subs
const User = require('../src/models/user_tg')
const Subscription = require('../src/models/subscription_tg')
const parseSeasonvarPage = require('../src/parsers/parse_seasonvar')

const MAX_FREE_SERIALS = 2

const defaultName = 'Мистер Робот'
const defaultUrl = 'http://seasonvar.ru/serial-11787-Mister_Robot-1-sezon.html'

describe('Checking message sending', () => {
  let bot
  let errorBot
  let fan
  let error

  beforeEach(() => {
    bot = {
      sendMessage: sinon.stub()
    }

    errorBot = {
      sendMessage: (message) => {
        console.log(message)
      }
    }

    fan = {
      id: 2,
      save: sinon.stub()
    }

    error = {
      toString: () => { return 'Error: ETELEGRAM: 403' }
    }
  })

  it('should handle common errors', (done) => {
    bot.sendMessage.rejects()

    const userInterface = new UI(bot, errorBot, {}, {})

    userInterface.sendMessage(fan, null, null)
      .then(() => {
        expect(bot.sendMessage.called).to.be.equal(true)
        expect(fan.save.called).to.be.equal(false)
        done()
      })
  })

  it('should handle user block', (done) => {
    bot.sendMessage.rejects(error)
    fan.save.resolves()

    const userInterface = new UI(bot, errorBot, {}, {})
    userInterface.sendMessage(fan, null, null)
      .then(() => {
        expect(bot.sendMessage.called).to.be.equal(true)
        expect(fan.save.called).to.be.equal(true)
        done()
      })
  })

  it('should handle user block and save error', (done) => {
    /* Checking that there no unhandled exceptions */
    bot.sendMessage.rejects(error)
    fan.save.rejects()

    const userInterface = new UI(bot, errorBot, {}, {})
    userInterface.sendMessage(fan, null, null)
      .then(() => {
        expect(bot.sendMessage.called).to.be.equal(true)
        expect(fan.save.called).to.be.equal(true)
        done()
      })
  })
})

describe('Checking sending simple answer', () => {
  let bot
  let errorBot
  let model
  let msg
  let stubUser
  const ID = 2

  beforeEach((done) => {
    bot = {}

    errorBot = {
      sendMessage: (message) => {
        console.log(message)
      }
    }

    model = {
      findOne: sinon.stub()
    }

    msg = {
      chat: {
        id: ID
      }
    }

    stubUser = new User({
      id: ID,
      name: 'Test',
      username: 'Testing',
      active: 1,
      subscriptions: 0
    })

    stubUser.save()
      .then(() => {
        done()
      })
  })

  it('should send register message if cant find user in db', (done) => {
    model.findOne.resolves(null)
    bot.sendMessage = (fan, message, opts) => {
      return new Promise((resolve, reject) => {
        /* fan is not a object because it's bot function */
        expect(fan).to.be.equal(ID)
        expect(message).to.be.equal(UserNotExist)
        done()
      })
    }

    const userInterface = new UI(bot, errorBot, model, {})
    userInterface.sendSimpleAnswer(msg, null, null)
  })

  it('should send message to user', (done) => {
    const message = 'Check'
    bot.sendMessage = sinon.stub()

    const userInterface = new UI(bot, errorBot, User, {})
    /* stubing UI function */
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(message)
      expect(user._id.toString()).to.be.equal(stubUser._id.toString()) /* exact same */

      done()
    })

    userInterface.sendSimpleAnswer(msg, message, null)
  })
})

describe('Checking /start command', () => {
  const ID = 3
  const username = 'username'
  let bot
  let errorBot
  let msg
  let stubUser

  beforeEach(() => {
    bot = {
      sendMessage: sinon.stub()
    }

    errorBot = {
      sendMessage: (message) => {
        console.log(message)
      }
    }

    msg = {
      chat: {
        id: ID,
        username: username
      }
    }

    stubUser = new User({
      id: ID,
      username: username,
      active: 0,
      subscriptions: 0
    })
  })

  it('should register new user', (done) => {
    const userInterface = new UI(bot, errorBot, User, {})
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(`${username} вы были зарегистрированны в системе, теперь вам нужно добавить отслеживаемые вами сериалы. Просто введите название интересующего вас сериала. Все вопросы вы можете задать здесь: https://t.me/swatcher_group_origin`)
      expect(user.active).to.be.equal(1)
      expect(user.isNew).to.be.equal(false)

      done()
    })

    userInterface.startUser(msg, null)
  })

  it('should return old one', (done) => {
    stubUser.save()
      .then(() => {
        const userInterface = new UI(bot, errorBot, User, {})
        sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
          expect(user.active).to.be.equal(1)
          expect(answer).to.be.equal(`${username} я рад что вы снова с нами!`)

          done()
        })

        userInterface.startUser(msg, null)
      })
  })

  it('should do nothing if already registed', (done) => {
    stubUser.active = 1
    stubUser.save()
      .then(() => {
        const userInterface = new UI(bot, errorBot, User, {})
        sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
          expect(user.active).to.be.equal(1)
          expect(answer).to.be.equal(`${username} вы уже зарегистрированны в системе.`)

          done()
        })

        userInterface.startUser(msg, null)
      })
  })
})

describe('Checking /stop command', () => {
  const ID = 3
  const username = 'username'
  let bot
  let errorBot
  let msg

  beforeEach(() => {
    bot = {
      sendMessage: sinon.stub()
    }

    errorBot = {
      sendMessage: (message) => {
        console.log(message)
      }
    }

    msg = {
      chat: {
        id: ID,
        username: username
      }
    }
  })

  it('should do send message if user is not registered', (done) => {
    const userInterface = new UI(bot, errorBot, User, {})
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal('Вы не были зарегистрированы!')

      done()
    })

    userInterface.stopUser(msg, null)
  })

  it('should set active to 0 if user exists', (done) => {
    const stubUser = new User({
      id: ID,
      username: username,
      active: 1,
      subscriptions: 0
    })

    stubUser.save()
      .then(() => {
        const userInterface = new UI(bot, errorBot, User, {})
        sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
          expect(answer).to.be.equal('Что ж, пока!')

          done()
        })

        userInterface.stopUser(msg, null)
      })
  })
})

describe('Checking search command', () => {
  let msg
  const ID = 3
  const username = 'username'
  const bot = {
    sendMessage: sinon.stub(),
    sendPhoto: sinon.stub()
  }
  const errorBot = {
    sendMessage: (message) => {
      console.log(message)
    }
  }

  beforeEach((done) => {
    msg = {
      chat: {
        id: ID,
        username: username
      }
    }

    new User({
      id: ID,
      username: username,
      active: 1,
      subscriptions: 0
    }).save(() => {
      done()
    })
  })

  it('should offer registration if not registered', (done) => {
    const userInterface = new UI(bot, errorBot, User, {})
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(UserNotExist)

      done()
    })

    msg.chat.id = 4
    userInterface.searchUserSerial(msg, defaultName)
  })

  it('should send not found', (done) => {
    const userInterface = new UI(bot, errorBot, User, {})
    sinon.stub(userInterface, '_fuzzySearch').callsFake((user, serialName) => false)

    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal('К сожалению ничего не было найдено!')

      done()
    })

    userInterface.searchUserSerial(msg, defaultName)
  })

  it('should send serial if found exact match', (done) => {
    const userInterface = new UI(bot, errorBot, User, {})

    bot.sendPhoto.callsFake((user, img, opts) => {
      expect(img).to.be.equal('http://cdn.seasonvar.ru/oblojka/11787.jpg')
      expect(opts).to.be.not.equal(null)
    }).resolves(null)

    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal('Какой добавить?')

      done()
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then(() => {
        userInterface.searchUserSerial(msg, defaultName)
      })
  })
  it('should send serials if not found exact match', (done) => {
    const userInterface = new UI(bot, errorBot, User, {})

    bot.sendPhoto.callsFake((user, img, opts) => {
      expect(img).to.be.equal('http://cdn.seasonvar.ru/oblojka/11787.jpg')
      expect(opts).to.be.not.equal(null)
    }).resolves(null)

    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal('Какой добавить?')

      done()
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then(() => {
        userInterface.searchUserSerial(msg, 'Мистер')
      })
  })

  it('should call fuzzy search if not found', (done) => {
    const userInterface = new UI(bot, errorBot, User, {})

    bot.sendPhoto.callsFake((user, img, opts) => {
      expect(img).to.be.equal('http://cdn.seasonvar.ru/oblojka/11787.jpg')
      expect(opts).to.be.not.equal(null)
    }).resolves(null)

    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal('Какой добавить?')

      done()
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then(() => {
        return Serial.find({}, 'name').exec()
      })
      .then((serials) => {
        userInterface.setFuseList(serials)
        userInterface.searchUserSerial(msg, 'Мистер Работ')
      })
  })
})

describe('Checking _getMostPopular', () => {
  const bot = {
    sendMessage: sinon.stub(),
    sendPhoto: sinon.stub()
  }
  const errorBot = {
    sendMessage: (message) => {
      console.log(message)
    }
  }

  it('should return same array if length < max', (done) => {
    const array = [1, 2, 3, 4]
    const max = 5
    const userInterface = new UI(bot, errorBot, User, {})

    userInterface._getMostPopular(array, max)
      .then(result => {
        expect(result.length).to.be.equal(array.length)

        done()
      })
  })

  it('should return first max elements of array if there no subscriptions', (done) => {
    const additionalUrl = 'http://seasonvar.ru/serial-9701-Mister_Sloen.html'
    const additionalName = 'Мистер Слоэн'

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then(() => {
        return parseSeasonvarPage(additionalUrl, additionalName, false)
      })
      .then(() => {
        return Serial.find({ name: new RegExp('Мистер', 'i') })
      })
      .then((serials) => {
        const userInterface = new UI(bot, errorBot, User, Subscription)

        expect(serials.length).to.be.equal(2)

        return userInterface._getMostPopular(serials, 1)
      })
      .then((result) => {
        expect(result.length).to.be.equal(1)

        done()
      })
  })

  it('should return most popular by subscripFtions serials', (done) => {
    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then((serial) => {
        return new Subscription({
          serial: serial._id,
          fans: [{
            user: mongoose.Types.ObjectId('4edd40c86762e0fb12000005'),
            voiceover: []
          }]
        }).save()
      })
      .then(() => {
        return parseSeasonvarPage('http://seasonvar.ru/serial-22268-Ved_mak_2019.html', 'Ведьмак', false)
      })
      .then((serial) => {
        return new Subscription({
          serial: serial._id,
          fans: [{
            user: mongoose.Types.ObjectId('4edd40c86762e0fb12000004'),
            voiceover: []
          },
          {
            user: mongoose.Types.ObjectId('4edd40c86762e0fb12000003'),
            voiceover: []
          }
          ]
        }).save()
      })
      .then(() => {
        return Serial.find({ })
      })
      .then((serials) => {
        const userInterface = new UI(bot, errorBot, User, Subscription)

        expect(serials.length).to.be.equal(2)
        return userInterface._getMostPopular(serials, 1)
      })
      .then((result) => {
        expect(result.length).to.be.equal(1)
        expect(result[0].serial.name).to.be.equal('Ведьмак')

        done()
      })
  })
})

describe('Checking command add', () => {
  let msg
  let stubUser
  const ID = 3
  const username = 'username'
  const bot = {
    sendMessage: sinon.stub(),
    sendPhoto: sinon.stub()
  }
  const errorBot = {
    sendMessage: (message) => {
      console.log(message)
    }
  }

  beforeEach((done) => {
    msg = {
      chat: {
        id: ID,
        username: username
      }
    }

    stubUser = new User({
      id: ID,
      username: username,
      active: 1,
      subscriptions: 0,
      payed: 0
    })

    stubUser.save(() => {
      done()
    })
  })

  it('should offer to register if user not registered', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(UserNotExist)

      done()
    })

    msg.chat.id = 4
    userInterface.addUserSerial(msg, defaultName)
  })

  it('should offer payed subscription if limit exceed', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(FreeSubscribe)

      done()
    })

    stubUser.subscriptions = MAX_FREE_SERIALS + 1
    stubUser.save()
      .then(() => {
        return parseSeasonvarPage(defaultUrl, defaultName, false)
      })
      .then(() => {
        expect(stubUser.subscriptions).to.be.equal(MAX_FREE_SERIALS + 1)
        userInterface.addUserSerial(msg, ['', defaultName])
      })
  })

  it('should send message if can\'t find serial', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    const serialName = 'Мистер'

    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(`Не удалось однозначно идентифицировать ${serialName} для того что бы подписать вас, попробуйте еще раз или обратитесь за помощью в группу`)

      done()
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then(() => {
        userInterface.addUserSerial(msg, ['', serialName])
      })
  })

  it('should create new subscription entry if not exists', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)

    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(`Теперь вы подписаны на сериал: ${defaultName}`)

      Subscription.countDocuments({})
        .then((count) => {
          expect(count).to.be.equal(1)

          done()
        })
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then(() => {
        userInterface.addUserSerial(msg, ['', defaultName])
      })
  })

  it('should add fan to existing subscription', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)

    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(`Теперь вы подписаны на сериал: ${defaultName}`)

      Subscription.countDocuments({})
        .then((count) => {
          expect(count).to.be.equal(1)

          done()
        })
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then((serial) => {
        return new Subscription({
          serial: serial._id,
          fans: []
        }).save()
      })
      .then(() => {
        userInterface.addUserSerial(msg, ['', defaultName])
      })
  })

  it('should not add fan to existing subscription if fan already there', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)

    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(`Теперь вы подписаны на сериал: ${defaultName}`)

      Subscription.countDocuments({})
        .then((count) => {
          expect(count).to.be.equal(1)

          return Serial.findOne({ name: defaultName })
        })
        .then((serial) => {
          return Subscription.findOne({ serial: serial._id })
        })
        .then((subs) => {
          expect(subs.fans.length).to.be.equal(1)

          done()
        })
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then((serial) => {
        return new Subscription({
          serial: serial._id,
          fans: [{
            user: stubUser._id,
            voiceover: []
          }]
        }).save()
      })
      .then(() => {
        userInterface.addUserSerial(msg, ['', defaultName])
      })
  })

  it('should run _createContext if user payed and serial has voiceovers', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)

    const context = sinon.stub(userInterface, '_createContext')
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(`Выберете интересующие вас варианты озвучки сериала ${defaultName}`)
      expect(context.called).to.be.equal(true)

      done()
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then((serial) => {
        serial.voice_over.push('baibako')
        serial.voice_over.push('lostfilm')
        return serial.save()
      })
      .then(() => {
        stubUser.payed = 1
        return stubUser.save()
      })
      .then(() => {
        userInterface.addUserSerial(msg, ['', defaultName])
      })
  })

  it('should send message if user payed but serial doesn\'t have voiceovers', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)

    const context = sinon.stub(userInterface, '_createContext')
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(`Теперь вы подписаны на сериал: ${defaultName}, к сожалению в моей базе еще нет озвучек, выбрать озвучку можно будет после того как увидете первое уведомление о ней`)
      expect(context.called).to.be.equal(false)

      done()
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then(() => {
        stubUser.payed = 1
        return stubUser.save()
      })
      .then(() => {
        userInterface.addUserSerial(msg, ['', defaultName])
      })
  })
})

describe('Checking command remove', () => {
  let msg
  let stubUser
  const ID = 3
  const username = 'username'
  const bot = {
    sendMessage: sinon.stub(),
    sendPhoto: sinon.stub()
  }
  const errorBot = {
    sendMessage: (message) => {
      console.log(message)
    }
  }

  beforeEach((done) => {
    msg = {
      chat: {
        id: ID,
        username: username
      }
    }

    stubUser = new User({
      id: ID,
      username: username,
      active: 1,
      subscriptions: 0,
      payed: 0
    })

    stubUser.save(() => {
      done()
    })
  })

  it('should offer to register if user not registered', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(UserNotExist)

      done()
    })

    msg.chat.id = 4
    userInterface.removeUserSerials(msg, ['', defaultName])
  })

  it('should send message if there no serial', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(`Не удалось однозначно идентифицировать ${defaultName} для того что бы отписать вас, попробуйте еще раз или обратитесь за помощью в группу`)

      done()
    })

    userInterface.removeUserSerials(msg, ['', defaultName])
  })

  it('should do nothing if user not subscribed', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal('Вы не подписаны на этот сериал!')

      done()
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then(() => {
        userInterface.removeUserSerials(msg, ['', defaultName])
      })
  })

  it('should remove subscription', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(`Вы отписались от сериала: ${defaultName}`)

      done()
    })

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then((serial) => {
        return new Subscription({
          serial: serial._id,
          fans: [{
            user: stubUser._id,
            voiceover: []
          }]
        }).save()
      })
      .then(() => {
        userInterface.removeUserSerials(msg, ['', defaultName])
      })
  })
})

describe('Checking command list', () => {
  let msg
  let stubUser
  const ID = 3
  const username = 'username'
  const bot = {
    sendMessage: sinon.stub(),
    sendPhoto: sinon.stub()
  }
  const errorBot = {
    sendMessage: (message) => {
      console.log(message)
    }
  }

  beforeEach((done) => {
    msg = {
      chat: {
        id: ID,
        username: username
      }
    }

    stubUser = new User({
      id: ID,
      username: username,
      active: 1,
      subscriptions: 0,
      payed: 0
    })

    stubUser.save(() => {
      done()
    })
  })

  it('should offer to register if user not registered', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal(UserNotExist)

      done()
    })

    msg.chat.id = 4
    userInterface.listUserSerials(msg, ['', defaultName])
  })
  it('should send message if user doesn\'t have subscriptions', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal('Вы не подписаны не на один сериал')

      done()
    })

    userInterface.listUserSerials(msg, ['', defaultName])
  })

  it('should send subscriptions to user', (done) => {
    const userInterface = new UI(bot, errorBot, User, Subscription)
    sinon.stub(userInterface, 'sendMessage').callsFake((user, answer, opts) => {
      expect(answer).to.be.equal('Вот список сериалов на которые вы подписаны!')

      done()
    })

    bot.sendPhoto.resolves()

    parseSeasonvarPage(defaultUrl, defaultName, false)
      .then((serial) => {
        return new Subscription({
          serial: serial._id,
          fans: [{
            user: stubUser._id,
            voiceover: []
          }]
        }).save()
      })
      .then(() => {
        userInterface.listUserSerials(msg, ['', defaultName])
      })
  })
})
