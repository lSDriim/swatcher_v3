const { expect } = require('chai')
/* testing for Telegram, but other use same schema */
const User = require('../src/models/user_tg')
const Subscription = require('../src/models/subscription_tg')
const Serial = require('../src/models/serial')

describe('Checking clear subscriprion function', () => {
  let igor
  let ivan
  let subsGame
  let subsRobot
  let gameSerial
  let robotSerial

  beforeEach((done) => {
    robotSerial = new Serial({
      name: 'Мистер Робот'
    })

    gameSerial = new Serial({
      name: 'Игра Престолов'
    })

    robotSerial.save()
      .then(() => {
        return gameSerial.save()
      })
      .then(() => {
        igor = new User({
          name: 'Igor'
        })

        return igor.save()
      })
      .then(() => {
        ivan = new User({
          name: 'Ivan'
        })

        return ivan.save()
      })
      .then(() => {
        subsRobot = new Subscription({
          serial: robotSerial,
          fans: [{
            user: ivan
          }, {
            user: igor
          }]
        })

        return subsRobot.save()
      })
      .then(() => {
        subsGame = new Subscription({
          serial: gameSerial,
          fans: [{
            user: ivan
          }]
        })

        return subsGame.save()
      })
      .then(() => done())
  })

  it('should remove all subscriptions of user and not remove others', (done) => {
    expect(subsRobot.fans.length).to.be.equal(2)
    expect(subsGame.fans.length).to.be.equal(1)

    ivan.clearSubscriptions()
      .then(() => {
        return Subscription.findOne({ _id: subsRobot._id })
      })
      .then((subs) => {
        subsRobot = subs

        return Subscription.findOne({ _id: subsGame._id })
      })
      .then((subs) => {
        subsGame = subs

        expect(subsRobot.fans.length).to.be.equal(1)
        expect(subsGame.fans.length).to.be.equal(0)

        done()
      })
  })
})
